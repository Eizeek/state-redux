import  { useState } from 'react';
import axios from '../../api/axios.js';
import { validate } from '../../validator/email-validator';
import './join.css';

export const JoinProgram = () => {
  const [email, setEmail] = useState('');
  const [subscribed, setSubscribed] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleSubscribe = async (e) => {
    e.preventDefault();

    if (!email || !validate(email)) {
      alert('Invalid email address');
      return;
    }

    if (loading) {
      return;
    }

    setLoading(true);

    try {
      await axios.post('/subscribe', { email });
      setSubscribed(true);
    } catch (error) {
      if (error.response && error.response.status === 422) {
        alert('Email is already in use');
      } else {
        alert('An error occurred while subscribing');
      }
    } finally {
      setLoading(false);
    }
  };

  const handleUnsubscribe = async (e) => {
    e.preventDefault();

    if (!email || !validate(email)) {
      alert('Invalid email address');
      return;
    }

    if (loading) {
      return;
    }

    setLoading(true);

    try {
      await axios.post('/unsubscribe', { email });
      setSubscribed(false);
      setEmail(''); 
    } catch (error) {
      alert('An error occurred while unsubscribing');
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className='join-our-program'>
      <h1>JOIN OUR PROGRAM</h1>

      <h2>
        We are proud of our products, and we are really excited <br />
        when we get feedback from our users.
      </h2>

      <form>
        {!subscribed && ( 
          <input
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            disabled={loading}
          />
        )}
        {subscribed ? (
          <button onClick={handleUnsubscribe} disabled={loading}>
            UNSUBSCRIBE
          </button>
        ) : (
          <button onClick={handleSubscribe} disabled={loading}>
            SUBSCRIBE
          </button>
        )}
      </form>
    </div>
  );
};
