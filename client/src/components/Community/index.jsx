import { useEffect, useState } from "react";
import axios from "../../api/axios.js";
import "./Community.css";

export const Community = () => {
  const [data, setData] = useState([]);
  const [showSection, setShowSection] = useState(true); // State to control section visibility

  useEffect(() => {
    axios
      .get("/community")
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }, []);

  // Function to toggle section visibility
  const toggleSection = () => {
    setShowSection(!showSection);
  };

  return (
    <>
      <button
        className={`toggle ${showSection ? "" : "show-section"}`}
        onClick={toggleSection}
      >
        {showSection ? "Hide Section" : "Show Section"}
      </button>

      <section
        className={`app-section app-section_community ${
          showSection ? "show" : "hide"
        }`}
      >
        <h1 className="app-title">
          Big Community of
          <br />
          People Like You
        </h1>

        <h2 className="app-subtitle">
          We are proud of our products, and we are really excited <br />
          when we get feedback from our users.
        </h2>

        <div className="card">
          {data.map((user) => (
            <div key={user.id} className="app-section_cards_card">
              <img
                className="app-section_cards_card--img"
                src={user.avatar}
                alt=""
              />
              <p className="app-section_cards_card--p">
                Aliquip ex ea commodo consequat. Duis aute irure dolor in
                reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur.
              </p>
              <p className="app-section_cards_card--aut">
                {user.firstName} {user.lastName}
              </p>
              <p className="app-section_cards_card--info">{user.position}</p>
            </div>
          ))}
        </div>
      </section>
    </>
  );
};
